﻿DROP PROCEDURE IF EXISTS summing;
DROP TABLE IF EXISTS temp_otricat_vol;
DROP TABLE IF EXISTS temp_polozh_vol;
CREATE TEMPORARY TABLE temp_otricat_vol AS (
            SELECT *
            FROM data where volume<0 ORDER BY card_number,id);
           
CREATE TEMPORARY TABLE temp_polozh_vol AS (
            SELECT *
            FROM data where volume>0);
DELETE FROM data WHERE volume>0;
DELIMITER // 
CREATE PROCEDURE `summing` () 
    LANGUAGE SQL 
    DETERMINISTIC 
    SQL SECURITY DEFINER 
    COMMENT 'procedure' 
    BEGIN 
        DECLARE count_rows, counter, id_temp_1, id_temp_2, temp_card_no INT;
        DECLARE temp_vol, count_vozvrat numeric;
        
        SELECT count(*) INTO count_rows from temp_otricat_vol;

        SET counter =0;
        SET count_vozvrat=0;
        WHILE counter < count_rows DO
			
			SELECT id, card_number INTO id_temp_1, temp_card_no  FROM temp_otricat_vol LIMIT counter, 1;
			
            IF counter <> count_rows-1 THEN
				SET counter = counter+1;
				SELECT id INTO id_temp_2 FROM temp_otricat_vol LIMIT counter, 1; 
                
                IF id_temp_1<id_temp_2 THEN
					SELECT SUM(volume) INTO count_vozvrat FROM temp_polozh_vol WHERE (id BETWEEN id_temp_1 AND id_temp_2) AND card_number=temp_card_no;
				ELSE
					SELECT SUM(volume) INTO count_vozvrat FROM temp_polozh_vol WHERE (id BETWEEN id_temp_2 AND id_temp_1) AND card_number=temp_card_no;
                END IF;
            
						IF count_vozvrat > 0 THEN
						UPDATE data
						SET volume = volume + count_vozvrat
						WHERE id = id_temp_1;
						END IF;
            else
                SELECT SUM(volume) INTO count_vozvrat FROM temp_polozh_vol WHERE (id > id_temp_1) AND card_number=temp_card_no;

				IF count_vozvrat > 0 THEN
					UPDATE data
					SET volume = volume + count_vozvrat
					WHERE id = id_temp_1;
				END IF;                

                
                SET counter = counter+1; 
			END IF;
                
        END WHILE; 
END//

CALL summing();